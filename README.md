Instrucciones de la línea de comandos
También puede cargar archivos existentes desde su computadora siguiendo las instrucciones a continuación.

Configuración global de Git

git config --usuario global.nombre "Nicolás Vargas"<br>
git config --usuario global.email "navargasc@gmail.com"

Crear un nuevo repositorio

clon de git https://gitlab.com/o53g05paqueadero/o53g05parqueadero.git<br>
cd o53g05parqueadero<br>
interruptor git -c principal<br>
toque LÉAME.md<br>
git agregar README.md<br>
git commit -m "agregar LÉAME"
git push -u origen principal

Empujar una carpeta existente

cd carpeta_existente
git init --initial-branch=principal
git remoto agregar origen https://gitlab.com/o53g05paqueadero/o53g05parqueadero.git
agrega git
git commit -m "Commit inicial"
git push -u origen principal

Empuje un repositorio Git existente

cd existente_repo
git remoto renombrar origen origen antiguo
git remoto agregar origen https://gitlab.com/o53g05paqueadero/o53g05parqueadero.git 
git push -u origen --all
git push -u origen --etiquetas
